import { Browser, Page } from "puppeteer";

const puppeteer = require('puppeteer');
const LZP = require('./page');
let browser:Browser|null = null;
let page:Page|null = null;
let lzp: LZPage|null = null;
it('Adds 1 + 1 to equals 2', () => {
    expect(1 + 1).toBe(2);
});

describe('Lazycodet test', () => {
    beforeAll(async () => {
        browser = await puppeteer.launch({ headless: false, slowMo: 200 }) as Browser;
        page = await browser.newPage();
        lzp = new LZP(page);
        await page.setViewport({
            width: 1280,
            height: 720
        });
    });
    afterAll(async () => {
        if(browser)
            await browser.close();
    });
    beforeEach(async () => {
        // Hàm này sẽ được chạy mỗi khi 1 lênh it chạy
    }, 60000);
    it('Search something', async () => {
        expect.assertions(1);
        try {
            if(page)
            {
                await page.goto('https://lazycodet.com');
                const searchBox = await page.$('#search-main-window');
                if(searchBox)
                {
                    await searchBox.type('github');
                    await searchBox.press('Enter');
                    // await page.waitForNavigation();
                    const products = await lzp!.query('.post-box');
                    expect(products.length).toBeGreaterThan(0);
                }
                else console.log('searchBox not found');
            }
        } catch (error) {
            console.log(error);
        }
    }, 60000);
    
});