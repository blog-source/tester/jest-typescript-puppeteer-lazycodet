class LZPage {
    page;
    constructor(page: any)
    {
        this.page = page;
    }
    async query(selector: string)
    {
        await this.page.waitForSelector(selector);
        const result = await this.page.$$(selector);
        console.log(`Query ${selector}:`, result);
        return result;
    }
}

module.exports = LZPage;